package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;

import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.RectangleCollision;
import de.amadillo.entitysystem.components.RectangleColor;

public class DebugDrawRectanglesProcessor extends EntityProcessingSystem {

	ComponentMapper<Position> pm;
	ComponentMapper<RectangleCollision> rcm;
	ComponentMapper<RectangleColor> rcom;
	ShapeRenderer shapes;

	@SuppressWarnings("unchecked")
	public DebugDrawRectanglesProcessor(ShapeRenderer shapes) {
		super(Aspect.getAspectForAll(Position.class, RectangleCollision.class,
				RectangleColor.class));
		this.shapes = shapes;
	}

	@Override
	protected void initialize() {
		pm = world.getMapper(Position.class);
		rcm = world.getMapper(RectangleCollision.class);
		rcom = world.getMapper(RectangleColor.class);
	}

	@Override
	protected void begin() {
		shapes.begin(ShapeType.Line);
	}

	protected void process(Entity e) {
		Vector2 pos = pm.get(e).pos;
		Vector2 dim = rcm.get(e).dimension;
		shapes.setColor(rcom.get(e).color);

		shapes.rect(pos.x, pos.y, dim.x, dim.y);
	}

	@Override
	protected void end() {
		shapes.end();
	}

	@Override
	protected void dispose() {
		shapes = null;
	}
}
