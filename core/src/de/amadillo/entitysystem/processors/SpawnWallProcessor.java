package de.amadillo.entitysystem.processors;

import java.util.Iterator;
import java.util.LinkedList;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.math.Vector2;
import com.sun.org.glassfish.gmbal.GmbalException;

import de.amadillo.entitysystem.components.PlayerInput;
import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.Velocity;
import de.amadillo.entitysystem.tools.GameConstants;
import de.amadillo.entitysystem.tools.GameFactory;

public class SpawnWallProcessor extends EntityProcessingSystem {

	ComponentMapper<Position> pm;
	LinkedList<Entity> wallList = new LinkedList<Entity>();
	float lastHeight;

	@SuppressWarnings("unchecked")
	public SpawnWallProcessor() {
		super(Aspect.getAspectForAll(Position.class, PlayerInput.class));
	}
	
	@Override
	protected void initialize() {
		pm = world.getMapper(Position.class);
		wallList.add(GameFactory.createStaticWall(world, new Vector2(0, GameConstants.WALL_START_HEIGHT), new Vector2(4, GameConstants.WALL_THICKNESS)));
		wallList.add(GameFactory.createStaticWall(world, new Vector2(GameConstants.PLAYFIELD_WIDTH, GameConstants.WALL_START_HEIGHT), new Vector2(-4, GameConstants.WALL_THICKNESS)));
		lastHeight = GameConstants.PLAYFIELD_WIDTH;
	}

	protected void process(Entity e) {
		Position player_pc = pm.get(e);
		Iterator<Entity> it = wallList.iterator();
		while(it.hasNext()) {
			Entity nextWall = it.next();
			Position wall_pc = nextWall.getComponent(Position.class);
			if(player_pc.pos.y > wall_pc.pos.y){
				nextWall.deleteFromWorld();
				it.remove();
			}
		}
	}
	
	@Override
	protected void end() {
		while(wallList.size() < 30) {
			wallList.add(GameFactory.createStaticWall(world, new Vector2(0,lastHeight+GameConstants.WALL_SPACING_HEIGHT), new Vector2(4,GameConstants.WALL_THICKNESS)));
			wallList.add(GameFactory.createStaticWall(world, new Vector2(GameConstants.PLAYFIELD_WIDTH,lastHeight+GameConstants.WALL_SPACING_HEIGHT), new Vector2(-4,GameConstants.WALL_THICKNESS)));
			lastHeight += 5;
		}
	}
}
