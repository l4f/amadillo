package de.amadillo.entitysystem.components;

import com.artemis.Component;
import com.badlogic.gdx.graphics.Color;

public class RectangleColor extends Component {
	public Color color;

	public RectangleColor() {
	}

	public RectangleColor(Color color) {
		this.color = color;
	}
}
