package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;

import de.amadillo.entitysystem.components.Position;
import static de.amadillo.entitysystem.tools.GameConstants.*;

public class ResetCoordinatesProcessor extends EntityProcessingSystem {

	private Bag<Entity> positions = new Bag<Entity>();
	private ComponentMapper<Position> pm;
	private boolean moveNow;

	@SuppressWarnings("unchecked")
	public ResetCoordinatesProcessor() {
		super(Aspect.getAspectForAll(Position.class));
	}

	@Override
	protected void inserted(Entity e) {
		positions.add(e);
	}
	
	@Override
	protected void removed(Entity e) {
		positions.remove(e);
	}

	@Override
	protected void initialize() {
		pm = world.getMapper(Position.class);
	}

	@Override
	protected void begin() {
		moveNow = false;
		for (int i = 0; i < positions.size(); i++) {
			if (pm.get(positions.get(i)).pos.y > COORDINATE_RESET_Y) {
				moveNow = true;
			}
		}
	}

	protected void process(Entity e) {
		if (moveNow) {
			Position pc = pm.get(e);
			pc.pos.sub(0, COORDINATE_RESET_Y );
		}
	}
}
