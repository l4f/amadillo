package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.RectangleCollision;
import de.amadillo.entitysystem.components.Velocity;
import de.amadillo.entitysystem.tools.GameConstants;

public class PlayerContainmentProcessor extends EntityProcessingSystem {

	ComponentMapper<RectangleCollision> rcm;
	ComponentMapper<Position> pm;
	ComponentMapper<Velocity> vm;

	@SuppressWarnings("unchecked")
	public PlayerContainmentProcessor() {
		super(Aspect.getAspectForAll(Velocity.class, Position.class, RectangleCollision.class));
	}

	@Override
	protected void initialize() {
		rcm = world.getMapper(RectangleCollision.class);
		pm = world.getMapper(Position.class);
		vm = world.getMapper(Velocity.class);
	}

	protected void process(Entity e) {
		RectangleCollision rc = rcm.get(e);
		Position pc = pm.get(e);
		Velocity vc = vm.get(e);

		if (pc.pos.x < 0) {
			pc.pos.x = 0;
			vc.vel.x = 0;
		} else if(pc.pos.x+rc.dimension.x > GameConstants.PLAYFIELD_WIDTH){
			pc.pos.x = GameConstants.PLAYFIELD_WIDTH-rc.dimension.x;
			vc.vel.x = 0;
		}
	}
}
