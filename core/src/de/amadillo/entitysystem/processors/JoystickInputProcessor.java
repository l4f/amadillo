package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;

import de.amadillo.entitysystem.components.PlayerInput;
import de.amadillo.entitysystem.components.Velocity;
import de.amadillo.entitysystem.input.Joystick;

public class JoystickInputProcessor extends EntityProcessingSystem {

	ComponentMapper<PlayerInput> pim;

	Joystick joy;
	
	@SuppressWarnings("unchecked")
	public JoystickInputProcessor(Joystick joystick) {
		super(Aspect.getAspectForAll(PlayerInput.class, Velocity.class));
		joy = joystick;
	}

	@Override
	protected void initialize() {
		pim = world.getMapper(PlayerInput.class);
	}

	protected void process(Entity e) {
		PlayerInput pic = pim.get(e);
		
		pic.dir = joy.getDirection();
		pic.magnitude = joy.getDeflection();
	}
}
