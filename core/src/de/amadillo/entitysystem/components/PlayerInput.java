package de.amadillo.entitysystem.components;

import com.artemis.Component;
import com.badlogic.gdx.math.Vector2;

public class PlayerInput extends Component {
	public Vector2 dir = new Vector2();
	public float magnitude = 0f;
}
