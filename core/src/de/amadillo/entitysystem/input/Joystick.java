package de.amadillo.entitysystem.input;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.Viewport;

public class Joystick {
	private ShapeRenderer sr;
	private Viewport viewport;

	private float outerRadius;
	private float innerRadius;
	private float cornerDistance;

	private Vector3 joystickCenterView = new Vector3();
	private Vector2 joystickCenterWorld = new Vector2();
	private Vector2 joystickStickCenterWorld = new Vector2();

	public Joystick(ShapeRenderer sr, Viewport viewport) {
		this.viewport = viewport;
		this.sr = sr;
	}

	public void update() {
		final float stwr_x = viewport.getWorldWidth() / (float) viewport.getScreenWidth();
		outerRadius = viewport.getScreenWidth() / 15 * stwr_x;
		innerRadius = viewport.getScreenWidth() / 30 * stwr_x;
		cornerDistance = viewport.getScreenWidth() / 10;

		joystickCenterView.set(Gdx.graphics.getWidth() - cornerDistance,
				Gdx.graphics.getHeight() - cornerDistance, 0);
		Vector3 pos = viewport.unproject(joystickCenterView.cpy());
		joystickCenterWorld = new Vector2(pos.x, pos.y);
		Vector2 mousePos = new Vector2(Gdx.input.getX(), Gdx.input.getY());
		Vector3 mousePosGame = viewport.unproject(new Vector3(mousePos.x, mousePos.y, 0)).sub((new Vector3(joystickCenterWorld.x, joystickCenterWorld.y, 0)));
		joystickStickCenterWorld = new Vector2(mousePosGame.x, mousePosGame.y).limit(outerRadius).add(joystickCenterWorld);
	}
	
	public void draw() {
		
		float knobScale = (1f-getDeflection()*0.2f);
		
		sr.begin(ShapeType.Filled);

		//Draw base background
		sr.setColor(Color.valueOf("3D3D3D"));
		sr.circle(joystickCenterWorld.x, joystickCenterWorld.y, outerRadius, 32);
		//Draw base foreground
		sr.setColor(Color.valueOf("2B2B2B"));
		sr.circle(joystickCenterWorld.x, joystickCenterWorld.y, outerRadius*0.9f, 32);

		//Draw bearing ring
		sr.setColor(Color.valueOf("3D3D3D"));
		sr.circle(joystickCenterWorld.x, joystickCenterWorld.y, outerRadius*0.4f, 32);

		//Draw mount and bearing background
		sr.setColor(Color.valueOf("B00000"));
		sr.rectLine(joystickCenterWorld, joystickStickCenterWorld, innerRadius*0.6f);
		sr.circle(joystickCenterWorld.x,joystickCenterWorld.y, innerRadius*0.3f, 16);
		//Draw Mount and bearing foreground
		sr.setColor(Color.valueOf("C00000"));
		sr.rectLine(joystickCenterWorld, joystickStickCenterWorld, innerRadius*0.5f);
		sr.circle(joystickCenterWorld.x,joystickCenterWorld.y, innerRadius*0.25f, 16);

		//Draw ball background
		sr.setColor(Color.valueOf("D60000"));
		sr.circle(joystickStickCenterWorld.x, joystickStickCenterWorld.y, innerRadius*knobScale, 16);
		//Draw ball foreground
		sr.setColor(Color.valueOf("FF0000"));
		sr.circle(joystickStickCenterWorld.x, joystickStickCenterWorld.y, innerRadius*0.9f*knobScale, 16);

		//Draw ball specular
		sr.setColor(Color.valueOf("FF1919"));
		sr.circle(joystickStickCenterWorld.x-innerRadius*0.25f, joystickStickCenterWorld.y-innerRadius*0.25f, innerRadius*0.25f*knobScale, 8);
		sr.end();
	}

	public Vector2 getDirection() {
		return joystickStickCenterWorld.cpy().add(joystickCenterWorld.cpy().rotate(180)).nor();
	}

	public float getDeflection() {
		return joystickStickCenterWorld.cpy().add(joystickCenterWorld.cpy().scl(-1f)).len()/outerRadius;
	}
}
