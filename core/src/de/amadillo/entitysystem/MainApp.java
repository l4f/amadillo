package de.amadillo.entitysystem;

import static de.amadillo.entitysystem.tools.GameConstants.MAX_ZOOM;
import static de.amadillo.entitysystem.tools.GameConstants.MIN_ZOOM;
import static de.amadillo.entitysystem.tools.GameUtils.clamp;

import com.artemis.World;
import com.badlogic.gdx.Application.ApplicationType;
import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.input.GestureDetector;
import com.badlogic.gdx.input.GestureDetector.GestureListener;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Scaling;
import com.badlogic.gdx.utils.viewport.ScalingViewport;

import de.amadillo.entitysystem.input.Joystick;
import de.amadillo.entitysystem.processors.AmadilloCameraProcessor;
import de.amadillo.entitysystem.processors.DebugDrawRectanglesProcessor;
import de.amadillo.entitysystem.processors.PlayerContainmentProcessor;
import de.amadillo.entitysystem.processors.JoystickInputProcessor;
import de.amadillo.entitysystem.processors.MovementProcessor;
import de.amadillo.entitysystem.processors.PlayerAccelerationProcessor;
import de.amadillo.entitysystem.processors.RectangleCollisionProcessor;
import de.amadillo.entitysystem.processors.ResetCoordinatesProcessor;
import de.amadillo.entitysystem.processors.SpawnWallProcessor;
import de.amadillo.entitysystem.tools.GameConstants;
import de.amadillo.entitysystem.tools.GameFactory;

public class MainApp extends ApplicationAdapter implements GestureListener {

	/**Viewport*/
	private ScalingViewport viewport;
	/**Camera*/
	private Camera camera;
	/**Shape-renderer*/
	private ShapeRenderer shapes;
	/**Touchscreen-gesture detection*/
	private GestureDetector gesture;
	/**Onscreen-joystick*/
	private Joystick joystick;

	/** entity-world*/
	private World world;

	/** Screensize in pixels*/
	private int sw, sh;
	/** viewport-size and world-size, as well as height to width ratio [h/w] */
	private float vw, vh, ww, wh, ratio;
	/** screen to world ratio in x/y direction*/
	private float stwr_x, stwr_y;

	/** camera scroll speed*/
	private float speed = 1f;

	/**Is called upon re-/opening the game*/
	@Override
	public void create() {
		viewport = new ScalingViewport(Scaling.fillX, ww, wh, camera = new OrthographicCamera());

		gesture = new GestureDetector(this);

		shapes = new ShapeRenderer(64000);

		world = new World();

		resize(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		joystick = new Joystick(shapes, viewport);

		world.setSystem(new ResetCoordinatesProcessor());
		world.setSystem(new SpawnWallProcessor());
		world.setSystem(new JoystickInputProcessor(joystick));
		world.setSystem(new PlayerAccelerationProcessor());
		world.setSystem(new MovementProcessor());
		world.setSystem(new PlayerContainmentProcessor());
		world.setSystem(new RectangleCollisionProcessor());
		world.setSystem(new AmadilloCameraProcessor(camera));
		world.setSystem(new DebugDrawRectanglesProcessor(shapes));

		world.initialize();

		GameFactory.createPlayer(world, new Vector2(GameConstants.PLAYFIELD_WIDTH/2f,0f));
		
		Gdx.input.setInputProcessor(new InputMultiplexer());
	}

	/**Render the game*/
	@Override
	public void render() {
		//Set projection-matrix
		shapes.setProjectionMatrix(camera.combined);

		//Clear Screen
		Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		//Process Game-Logic
		act(Gdx.graphics.getRawDeltaTime());

		//Draw World Bounds
		drawBounds();
		
		//Draw Joystick
		joystick.draw();
	}

	/**Draw world bounds*/
	private void drawBounds() {
		// draw in red: a big rectangle to show the coordinate-range that is safe to use
		shapes.begin(ShapeType.Line);
		shapes.setColor(Color.RED);

		shapes.rect(-ww, -wh, 2*ww, 2*wh);
		
		shapes.end();
	}

	/**Process game logic*/
	private void act(float delta) {
		handleInput();
		world.setDelta(Gdx.graphics.getDeltaTime());
		world.process();
	}

	/** Handle input detection */
	private void handleInput() {
		// Handle Joystick input detection
		joystick.update();

		// Zoom view in desktop mode
		if (Gdx.app.getType() == ApplicationType.Desktop) {
			// Exit app in desktop mode
			if (Gdx.input.isKeyPressed(Keys.ESCAPE)) {
				Gdx.app.exit();
			}
			if (Gdx.input.isKeyPressed(Keys.PAGE_UP)) {
				zoomView(1 - 2f * Gdx.graphics.getRawDeltaTime());
			} else if (Gdx.input.isKeyPressed(Keys.PAGE_DOWN)) {
				zoomView(1f + 2f * Gdx.graphics.getRawDeltaTime());
			}
		}
	}

	/**
	 * Zooms by the given amount in percent relative to the current zoom
	 **/
	private void zoomView(float percent) {
		vw = clamp(MIN_ZOOM, vw * percent, MAX_ZOOM);
		vh = vw * ratio;
		viewport.setWorldSize(vw, vh);
		viewport.update(sw, sh);
		stwr_x = vw / (float) sw;
		stwr_y = vh / (float) sh;
	}

	/**
	 * Moves the Camera with a certain speed in the direction the pointer is offset from the center of the screen
	 * @param delta The last delta value 
	 **/
	
	@SuppressWarnings("unused")
	private void moveCamToMouse(float delta) {
		Vector3 mouseWorldPos = camera.unproject(new Vector3(sw - Gdx.input.getX(), sh - Gdx.input.getY(), 0));
		Vector3 screenMid = camera.unproject(new Vector3(sw / 2, sh / 2, 0));
		Vector3 pointerDelta = screenMid.sub(mouseWorldPos);
		float dist = pointerDelta.len();
		Vector3 moveCamDelta = pointerDelta.nor().scl(clamp(0f, speed * dist, vw));
		moveView(moveCamDelta.scl(delta));
	}

	/**Move the camera by the parameter v*/
	private void moveView(Vector3 v) {
		camera.translate(v);
		camera.update();
	}

	/**Setup the viewport*/
	@Override
	public void resize(int width, int height) {
		sw = width;
		sh = height;

		ratio = (float) sh / (float) sw;

		ww = 10000;
		wh = 10000;

		vw = GameConstants.PLAYFIELD_WIDTH;
		vh = vw*ratio;


		stwr_x = vw / (float) sw;
		stwr_y = vh / (float) sh;

		viewport.setWorldSize(vw, vh);
		viewport.update(width, height, true);
	}

	/**Clean up the resources*/
	@Override
	public void dispose() {
		world.dispose();
		shapes.dispose();
	}

	@Override
	public boolean touchDown(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean tap(float x, float y, int count, int button) {
		return false;
	}

	@Override
	public boolean longPress(float x, float y) {
		return false;
	}

	@Override
	public boolean fling(float velocityX, float velocityY, int button) {
		return false;
	}

	@Override
	public boolean pan(float x, float y, float deltaX, float deltaY) {
		//moveView(new Vector3(-deltaX * stwr_x, deltaY * stwr_y, 0));
		return false;
	}

	@Override
	public boolean panStop(float x, float y, int pointer, int button) {
		return false;
	}

	@Override
	public boolean zoom(float initialDistance, float distance) {
		//zoomView(initialDistance / distance <= 1 ? 0.98f : 1.02f);
		return false;
	}

	@Override
	public boolean pinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
		return false;
	}
}
