package de.amadillo.entitysystem.components;

import com.artemis.Component;
import com.artemis.Entity;
import com.badlogic.gdx.math.Vector2;

public class RectangleCollision extends Component {
	public Vector2 dimension = new Vector2();
	public Entity owner;

	public RectangleCollision() {
	}

	public RectangleCollision(Vector2 d, Entity owner) {
		dimension.set(d);
		this.owner = owner;
	}
}
