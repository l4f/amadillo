package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.badlogic.gdx.graphics.Camera;

import de.amadillo.entitysystem.components.PlayerInput;
import de.amadillo.entitysystem.components.Position;

public class AmadilloCameraProcessor extends EntityProcessingSystem {

	ComponentMapper<Position> pm;
	Camera cam;

	@SuppressWarnings("unchecked")
	public AmadilloCameraProcessor(Camera camera) {
		super(Aspect.getAspectForAll(PlayerInput.class, Position.class));
		this.cam = camera;
	}

	@Override
	protected void initialize() {
		pm = world.getMapper(Position.class);
	}

	protected void process(Entity e) {
		cam.position.y = pm.get(e).pos.y+cam.viewportHeight*0.25f;
		cam.update();
	}
}
