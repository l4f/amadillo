package de.amadillo.entitysystem.tools;

public class GameConstants {

	private GameConstants() {
	}
	/** The maximal and minimal zoom values */
	public final static float MIN_ZOOM = 0.0001f, MAX_ZOOM = 100000f;

	/** The maximal and minimal zoom values */
	public final static float COORDINATE_RESET_Y = 1000f;

	/** Width of the area that the player can move in */
	public final static float PLAYFIELD_WIDTH = 10f;

	/** TODO */
	public final static float WALL_START_HEIGHT = 5f;

	/** TODO */
	public final static float WALL_SPACING_HEIGHT = 3f;

	/** TODO */
	public final static float WALL_SPACING_GAP = 2f;

	/** TODO */
	public final static float WALL_THICKNESS = 1f;

	/** The maximum speed of the player in m/s */
	public final static float PLAYER_SPEED = 5f;

	/** The maximum acceleration of the player in m/s^2 */
	public static final float PLAYER_ACCELERATION = 2f;
}
