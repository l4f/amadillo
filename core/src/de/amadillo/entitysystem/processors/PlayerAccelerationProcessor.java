package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;

import de.amadillo.entitysystem.components.PlayerInput;
import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.Velocity;
import de.amadillo.entitysystem.tools.GameConstants;

public class PlayerAccelerationProcessor extends EntityProcessingSystem {

	ComponentMapper<Velocity> vm;
	ComponentMapper<PlayerInput> pim;

	@SuppressWarnings("unchecked")
	public PlayerAccelerationProcessor() {
		super(Aspect.getAspectForAll(PlayerInput.class, Velocity.class));
	}

	@Override
	protected void initialize() {
		vm = world.getMapper(Velocity.class);
		pim = world.getMapper(PlayerInput.class);
	}

	protected void process(Entity e) {
		Velocity vc = vm.get(e);
		PlayerInput pic = pim.get(e);

		vc.vel.add(pic.dir.cpy().scl(pic.magnitude*GameConstants.PLAYER_ACCELERATION*world.getDelta()));
		vc.vel.clamp(0f, GameConstants.PLAYER_SPEED);
	}
}
