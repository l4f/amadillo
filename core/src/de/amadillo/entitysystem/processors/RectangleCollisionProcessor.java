package de.amadillo.entitysystem.processors;

import com.artemis.Aspect;
import com.artemis.ComponentMapper;
import com.artemis.Entity;
import com.artemis.systems.EntityProcessingSystem;
import com.artemis.utils.Bag;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

import de.amadillo.entitysystem.components.ActiveCollision;
import de.amadillo.entitysystem.components.PassiveCollision;
import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.RectangleCollision;
import de.amadillo.entitysystem.components.RectangleCollisionEvent;

public class RectangleCollisionProcessor extends EntityProcessingSystem {

	private ComponentMapper<Position> pm;
	private ComponentMapper<RectangleCollision> rcm;
	private ComponentMapper<ActiveCollision> acm;

	private Bag<Entity> collidablesBag = new Bag<Entity>();

	Rectangle testRectangle = new Rectangle();
	Rectangle otherRectangle = new Rectangle();

	@SuppressWarnings("unchecked")
	public RectangleCollisionProcessor() {
		super(Aspect.getAspectForAll(RectangleCollision.class, Position.class)
				.one(ActiveCollision.class, PassiveCollision.class));
	}

	@Override
	protected void initialize() {
		pm = ComponentMapper.getFor(Position.class, world);
		rcm = ComponentMapper.getFor(RectangleCollision.class, world);
		acm = ComponentMapper.getFor(ActiveCollision.class, world);
	}

	@Override
	protected void inserted(Entity e) {
		collidablesBag.add(e);
	}

	@Override
	protected void removed(Entity e) {
		collidablesBag.remove(e);
	}

	protected void process(Entity e) {
		if (acm.getSafe(e) == null) {
			return;
		} else {

			Vector2 posT = pm.get(e).pos;
			Vector2 dimT = rcm.get(e).dimension;
			testRectangle.set(posT.x, posT.y, dimT.x, dimT.y);

			for (int i = 0; i < collidablesBag.size(); i++) {
				for (int j = 0; j < collidablesBag.size(); j++) {

					Vector2 posO = pm.get(e).pos;
					Vector2 dimO = rcm.get(e).dimension;
					otherRectangle.set(posO.x, posO.y, dimO.x, dimO.y);

					if (testRectangle.overlaps(otherRectangle)) {
						e.edit().add(new RectangleCollisionEvent(rcm.get(e)));
					}
				}
			}
		}
	}
}
