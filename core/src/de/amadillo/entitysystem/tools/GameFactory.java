package de.amadillo.entitysystem.tools;

import com.artemis.Entity;
import com.artemis.World;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Vector2;

import de.amadillo.entitysystem.components.PlayerInput;
import de.amadillo.entitysystem.components.Position;
import de.amadillo.entitysystem.components.RectangleCollision;
import de.amadillo.entitysystem.components.RectangleColor;
import de.amadillo.entitysystem.components.Velocity;

public class GameFactory {

	private GameFactory() {
	}

	public static Entity createStaticWall(World world, Vector2 position, Vector2 dimension) {
		Entity e = world.createEntity();
		return e.edit()
				.add(new Position(position))
				.add(new RectangleCollision(new Vector2(dimension.x, dimension.y), e))
				.add(new RectangleColor(new Color(Color.BLUE)))
		.getEntity();
	}

	public static Entity createPlayer(World world, Vector2 position) {
		Entity e = world.createEntity();
		return e.edit()
				.add(new PlayerInput())
				.add(new Position(position))
				.add(new Velocity())
				.add(new RectangleCollision(new Vector2(0.3f,0.3f), e))
				.add(new RectangleColor(new Color(Color.GREEN)))
		.getEntity();
	}
}
