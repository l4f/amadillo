package de.amadillo.entitysystem.components;

import com.artemis.Component;

public class RectangleCollisionEvent extends Component {
	RectangleCollision r;

	public RectangleCollisionEvent() {
	}

	public RectangleCollisionEvent(RectangleCollision r) {
		this.r = r;
	}
}
